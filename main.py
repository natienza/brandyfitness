#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""main.py: Principal file of the training editor GUI"""

__author__ = "Nicolas ATIENZA, Sully MARIGLIANO"
__credits__ = ["Nicolas ATIENZA, SUlly MARIGLIANO"]
__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "nicolas.atienza@nisah.fr"
__status__ = "Development"

import src.mainWindow as window
import PyQt5.QtWidgets as qtw
import qdarkstyle
import sys
import importlib
importlib.reload(window)


if __name__ == '__main__':
    try:
        app
    except:
        app = qtw.QApplication(sys.argv)
        app.setStyleSheet(qdarkstyle.load_stylesheet_pyqt5())
        exMain = window.MainFenetre()
        sys.exit(app.exec_())
