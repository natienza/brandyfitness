![ici un joli logo de sully](logo/intro.png)

Brandy's Fitness
==

Petit application graphique produisant des entrainements de fitness selon un certains nombres de critères.
/!\ La BDD n'est actuellement pas en ligne

Dépendances
--

### poppler-qt5 #

Le résultat est sous forme de fichier PDF. Un rendu graphique de ce fichier est proposé dans l'application. La gestion du PDF est géré par la librairie `python-poppler-qt5` s'appuyant sur l'api C `poppler-qt5`. 

* Installation sous linux :

L'installation de la librairie se fait par l'installation du paquet `python-poppler-qt5` normalement présent sur les dépots officiels :

    ~# sudo apt install python-poppler-qt5
    
* Installation sous Windows :

L'installation de la librairie python peut s'effectuer par le gestionnaire de paquets `pip` :

    C:/ pip install python-poppler-qt5

Cependant, la librairie python dépend de l'api C `poppler-qt5` qui doit être installé sur votre machine. Aucune solution pertinente n'a été trouvé à ce jours.

Les dépendances sont : `sip`, `poppler`, `PyQt5`, `Qt5`

### mysql.connector #

Les requêtes SQL sont gérées par la biblitothèques `mysql.connector`

* Installation sous linux :

L'installation se fait à partir du gestionnaire de paquest du systeme :

    sudo apt install python3-mysql.connector


