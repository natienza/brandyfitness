#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""musclePopUp.py: Creation of a pop up window to choose muscles"""

__author__ = "Nicolas ATIENZA, Sully MARIGLIANO"
__credits__ = ["Nicolas ATIENZA, SUlly MARIGLIANO"]
__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "nicolas.atienza@nisah.fr"
__status__ = "Development"

from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import Qt, QTimer, QSize

import PyQt5.QtWidgets as qtw
import mysql.connector as db
import sys

class CheckList(qtw.QWidget):

    def __init__(self):

        # Constructeur parent
        qtw.QWidget.__init__(self)

        # Attributs
        self.boutons = []
        self.items = []
        self.layout = qtw.QVBoxLayout()
        self.setLayout(self.layout)

    def addItems(self, items):
        for item in items:
            self.boutons.append(qtw.QCheckBox(item))
            self.items.append(item)
        self.update()
    def addItem(self, item):
        self.boutons.append(qtw.QCheckBox(item))
        self.items.append(item)
        self.layout.addWidget(self.boutons[-1])

    def update(self):
        for b in self.boutons:
            self.layout.addWidget(b)

    def getItems(self):
        result = []
        for i in range(len(self.items)):
            if self.boutons[i].isChecked():
                result.append(self.items[i])
        return result

    def chekAll(self):
        for b in self.boutons:
            b.setChecked(True)

class CentralWindowTabulated(qtw.QTabWidget):

    def __init__(self,items, parent=None):

        # Constructeur parent
        qtw.QTabWidget.__init__(self)

        # Définition des tabulations
        self.tabs = []
        for i in items:
            self.tabs.append(CheckList())
            self.addTab(self.tabs[-1], i)

        # Remplissage des tabs
        try :
            for i in range(len(items)):
                parent.DB.execute('SELECT Muscle.nom FROM Muscle INNER JOIN GroupeMusculaire ON Muscle.groupe=GroupeMusculaire.id WHERE GroupeMusculaire.nom="' + items[i] + '"')
                for result in parent.DB:
                    self.tabs[i].addItem(result[0])
                self.tabs[i].chekAll()
        except :
            parent.statusBar().showMessage("Erreur de connexion à la BD")

    def getMuscleId(self, parent):
        muscleID = []
        for i in range(len(self.tabs)):
            for j in range(len(self.tabs[i].boutons)):
                if self.tabs[i].boutons[j].isChecked():
                    parent.DB.execute('select Muscle.id from Muscle where Muscle.nom="' + self.tabs[i].items[j] + '"')
                    for result in parent.DB:
                        muscleID.append(result[0])
        return muscleID

class MusclePopUp(qtw.QMainWindow):

    def __init__(self, items=None, parent=None):

        # Appel constructeur de la classe parent
        qtw.QMainWindow.__init__(self);

        # Attributs
        self.DB = None
        self.parent=parent

        # Caractéristiques de la fenêtre
        self.setWindowTitle("Choix des muscles")

        # Exit Action
        exitAction=qtw.QAction('&Exit', self)
        exitAction.setShortcut('Ctrl-Q')
        exitAction.setStatusTip("Quitter l'application")
        exitAction.triggered.connect(qtw.qApp.exit)


        # Init du bandeau menu
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('File')
        editMenu = mainMenu.addMenu('Edit')
        viewMenu = mainMenu.addMenu('View')
        searchMenu = mainMenu.addMenu('Search')
        toolsMenu = mainMenu.addMenu('Tools')
        helpMenu = mainMenu.addMenu('Help')
        exitButton = qtw.QAction('Exit', self)

        # Définition des actions du menuBar
        exitButton.setShortcut('Ctrl+Q')
        exitButton.setStatusTip('Exit application')
        exitButton.triggered.connect(self.close)
        fileMenu.addAction(exitButton)

        # Status Bar
        self.statusBar().showMessage('Prêt')

        # Boutons
        self.button1 = qtw.QPushButton("Annuler")
        self.button2 = qtw.QPushButton("Valider")

        self.button1.clicked.connect(self.close)
        self.button2.clicked.connect(self.sendMuscle)

        # CentralWindow
        self.tabWindow = CentralWindowTabulated(parent=self.parent, items=items)

        # Layout
        widget = qtw.QWidget(self)
        layout = qtw.QGridLayout()
        layout.addWidget(self.button1, 1,0)
        layout.addWidget(self.button2, 1, 1)
        layout.addWidget(self.tabWindow, 0,0, 1,0)
        widget.setLayout(layout)

        #Définition de la fenetre de travail
        if items==None or len(items)==0:
            self.setCentralWidget(qtw.QLabel("/!\ Pas de groupe musculaire sélectionné ! /!\ "))
        else:
            self.setCentralWidget(widget)
            #self.setCentralWidget(CentralWindowTabulated(parent=parent, items=items))

        # Lancement de la routine d'affichage de la fenetre
        self.resize(300, 300)
        self.show()

    def sendMuscle(self):
        self.statusBar().showMessage('Récupération des données de la BDD...')
        self.parent.muscleId = []
        self.parent.muscleId = self.tabWindow.getMuscleId(self.parent)
        self.statusBar().showMessage('Connecté')
        self.statusBar().showMessage('Fermeture')
        self.close()

if __name__ == '__main__':
    try:
        app
    except:
        app = qtw.QApplication(sys.argv)
        ex = MusclePopUp(['1', '2'])
        sys.exit(app.exec_())
