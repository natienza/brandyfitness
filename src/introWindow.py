#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""main.py: Principal file of the training editor GUI"""

__author__ = "Nicolas ATIENZA, Sully MARIGLIANO"
__credits__ = ["Nicolas ATIENZA, SUlly MARIGLIANO"]
__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "nicolas.atienza@nisah.fr"
__status__ = "Development"

from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import Qt, QTimer, QSize
import PyQt5.QtWidgets as qtw
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


class IntroWindow(qtw.QMainWindow):

    def __init__(self, parent=None):
        qtw.QMainWindow.__init__(self)
        self.setWindowTitle('Identification')

        # Widgets
        self.centralWidget = qtw.QLabel()
        self.email = qtw.QLineEdit()
        self.nom = qtw.QLineEdit()
        self.button1 = qtw.QPushButton('Annuler')
        self.button2 = qtw.QPushButton('Valider')
        self.check = qtw.QCheckBox("Je souhaite recevoir des informations de Brandy's Fitness")

        self.label = qtw.QLabel("Informations de contact")
        self.label.setFont(QtGui.QFont("Arial", 14, QtGui.QFont.Black))

        self.button2.setDisabled(True)
        self.button1.clicked.connect(self.close)
        self.button2.clicked.connect(self.send)
        self.email.editingFinished.connect(self.activateButton)
        # Layout
        layout = qtw.QGridLayout()
        layout.addWidget(self.label, 0,0, 1,-1)
        layout.addWidget(qtw.QLabel('email'),1,0, Qt.AlignCenter)
        layout.addWidget(self.email, 1,1, 1,-1)
        layout.addWidget(qtw.QLabel('nom'), 2,0, Qt.AlignCenter)
        layout.addWidget(self.nom, 2,1,1,-1) #layout.addWidget(self.check, 3,0, 3,3)
        layout.addWidget(self.button1, 4,2, Qt.AlignCenter)
        layout.addWidget(self.button2, 4,3, Qt.AlignCenter)

        self.centralWidget.setLayout(layout)
        self.setCentralWidget(self.centralWidget)
        self.resize(600, 200)
        self.statusBar().showMessage('Prêt')
        self.show()

    def send(self):
        self.statusBar().showMessage('Envoi du training par mail ...')
        server = smtplib.SMTP_SSL()
        self.statusBar().showMessage('Connexion au serveur mail...')
        server.connect('ssl0.ovh.net')
        self.statusBar().showMessage('ehlo...')
        server.ehlo()
        #self.statusBar().showMessage('Etablissement du lien TLS...')
        #server.starttls()
        self.statusBar().showMessage('login...')
        server.login('brandyfitness@nisah.fr', 'Brandy2010')
        fromaddr = "BRANDY's FITNESS <brandyfitness@nisah.fr>"
        toaddrs = [self.email.text()]
        sujet = "Fitness training"
        message = u""" iihttps://owncloud.nisah.fr/index.php/s/tSoQYFuZfQhUM3t
"""
        msg = """\
From: %s\r\n\
To: %s\r\n\
Subject: %s\r\n\
\r\n\
%s
""" % (fromaddr, ", ".join(toaddrs), sujet, message)
        message = MIMEMultipart()    ## Création de l'objet "message"
        message['From'] = fromaddr    ## Spécification de l'expéditeur
        message['To'] = toaddrs[0]    ## Attache du destinataire à l'objet "message"
        message['Subject'] = sujet    ## Spécification de l'objet de votre mail
        msg = "Bonjour,\nMerci d'avoir utilisé Brandy's Fitness training. Vous trouverez votre entrainement joint à ce mail. Bonne entrainement !!\n\nBrandy's Fitness team\n\n\n\nCeci est un mail automatique, merci de ne pas y répondre."    ## Message à envoyer
        message.attach(MIMEText(msg.encode('utf-8'), 'plain', 'utf-8'))    ## Attache du message à l'objet "message", et encodage en UTF-8
        nom_fichier = "training.pdf"    ## Spécification du nom de la pièce jointe
        piece = open("./out/training.pdf", "rb")    ## Ouverture du fichier
        part = MIMEBase('application', 'octet-stream')    ## Encodage de la pièce jointe en Base64
        part.set_payload((piece).read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', "piece; filename= %s" % nom_fichier)
        message.attach(part)    ## Attache de la pièce jointe à l'objet "message"
        try:
            self.statusBar().showMessage('Envoi du mail...')
            server.sendmail(fromaddr, toaddrs, message.as_string())
            self.statusBar().showMessage('Mail envoyé')
        except:
            self.statusBar().showMessage("Erreur lors de l'envoi du mail")
        server.quit()

    def activateButton(self):
        if '@' in self.email.text():
            self.statusBar().showMessage('Prêt')
            self.button2.setEnabled(True)
        else:
            self.statusBar().showMessage('email incorrect')
