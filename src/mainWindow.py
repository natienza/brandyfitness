#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""main.py: Principal file of the training editor GUI"""

__author__ = "Nicolas ATIENZA, Sully MARIGLIANO"
__credits__ = ["Nicolas ATIENZA, SUlly MARIGLIANO"]
__license__ = "GPL"
__version__ = "1.0.1"
__email__ = "nicolas.atienza@nisah.fr"
__status__ = "Development"

from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import Qt, QTimer, QSize
from src.musclePopUp import CheckList, MusclePopUp
import src.introWindow as intro
import PyQt5.QtWidgets as qtw
try :
    import popplerqt5 as pqt
    POPPLER_STATE = True
except :
    POPPLER_STATE = False
import mysql.connector as db
import qdarkstyle
import sys
import os
import importlib

importlib.reload(intro)

DPI_SCREEN = 72

class PDFWidget(qtw.QLabel):

    def __init__(self, filename='./out/training.pdf', pageNb=1, dpi=72, width=0, height=0):

        # Appel reconstruction constructeur
        qtw.QLabel.__init__(self)
        self.dpi=dpi
        self.filename=filename
        self.pageNb = pageNb

        # Chargement du fichier
        self.document = pqt.Poppler.Document.load(filename)
        self.document.setRenderHint(pqt.Poppler.Document.TextAntialiasing)
        self.page = self.document.page(pageNb)
        self.size = self.page.pageSize()
        # Carac de l'image
        if width != 0:
            self.dpi = int(DPI_SCREEN * (width/self.size.width()))
        img = self.page.renderToImage(self.dpi, self.dpi)
        # Déifinition de la zone d'affichage
        pixmap = QtGui.QPixmap(img)
        self.setPixmap(pixmap)


class ScrollPDFArea(qtw.QScrollArea):

    def __init__(self, filename='main.pdf'):

        # Constructeur parent
        qtw.QLabel.__init__(self)

        # Layout
        self.scroll_layout = qtw.QVBoxLayout()
        test = True
        pageNb = 0
        self.pages =[]
        while(test):
            try :
                self.pages.append((PDFWidget(pageNb = pageNb, width=self.width())))
                self.scroll_layout.addWidget(self.pages[-1])
                pageNb += 1
            except :
                self.nbPage = pageNb
                test=False
        self.content = qtw.QWidget()
        self.content.setLayout(self.scroll_layout)

        # Construction des éléments de la scrollArea
        self.setBackgroundRole(QtGui.QPalette.Dark)
        self.setWidget(self.content)

    def resizeEvent(self, event):
        self.scroll_layout = qtw.QVBoxLayout()
        test = True
        pageNb = 0
        self.pages =[]
        while(test):
            try :
                self.pages.append((PDFWidget(pageNb = pageNb, width=self.width())))
                self.scroll_layout.addWidget(self.pages[-1])
                pageNb += 1
            except :
                self.nbPage = pageNb
                test=False
        self.content = qtw.QWidget()
        self.content.setLayout(self.scroll_layout)

        # Construction des éléments de la scrollArea
        self.setBackgroundRole(QtGui.QPalette.Dark)
        self.setWidget(self.content)

    def update(self):
        self.scroll_layout = qtw.QVBoxLayout()
        test = True
        pageNb = 0
        self.pages =[]
        while(test):
            try :
                self.pages.append((PDFWidget(pageNb = pageNb, width=self.width())))
                self.scroll_layout.addWidget(self.pages[-1])
                pageNb += 1
            except :
                self.nbPage = pageNb
                test=False
        self.content = qtw.QWidget()
        self.content.setLayout(self.scroll_layout)

        # Construction des éléments de la scrollArea
        self.setBackgroundRole(QtGui.QPalette.Dark)
        self.setWidget(self.content)

class CaracEntrainement(qtw.QWidget):

    def __init__(self, parent=None):

        # Appel constructeur
        qtw.QWidget.__init__(self)
        self.parent = parent

        # Définition des Widgets
        self.dureeWidget=qtw.QComboBox()
        self.dureeWidget.addItems(['-', '20 min', '30 min', '45 min', '60 min'])

        self.muscleWidget=CheckList()
        try :
            parent.DB.execute("select nom from GroupeMusculaire")
            for result in parent.DB:
                self.muscleWidget.addItem(result[0])
        except :
            parent.statusBar().showMessage("Echec de connexion à la DB")
            self.muscleWidget.addItems(['-', 'Bras', 'Torse', 'Dorsaux', 'Fessiers', 'Jambe', 'Petit orteil'])

        slider=qtw.QSlider()
        slider.setFocusPolicy(Qt.StrongFocus)
        slider.setTickPosition(qtw.QSlider.TicksBothSides)
        slider.setTickInterval(10)
        slider.setSingleStep(1)
        slider = qtw.QSlider(Qt.Vertical)

        bouton=qtw.QPushButton("Choisir les muscles")
        bouton.clicked.connect(self.muscleChoice)

        # Définition du layout
        layout = qtw.QGridLayout()
        layout.addWidget(qtw.QLabel('Durée'), 0,0)
        layout.addWidget(self.dureeWidget, 1,0, 1, 1)
        layout.addWidget(qtw.QLabel('Groupe Musculaire à travailler'), 0,2)
        layout.addWidget(self.muscleWidget, 1,2, 3, 2)
        layout.addWidget(qtw.QLabel('Cardio'),2,0, Qt.AlignCenter)
        layout.addWidget(qtw.QLabel('Muscu'), 5,0, Qt.AlignCenter)
        layout.addWidget(slider,3,0, Qt.AlignCenter)
        layout.addWidget(bouton, 5,2)
        self.setLayout(layout)

    def muscleChoice(self):
        items = self.muscleWidget.getItems()
        self.parent.launchMuscleWindow(items=items)

class CaracEnv(qtw.QWidget):

    def __init__(self, parent = None):

        # Constructeur
        qtw.QWidget.__init__(self)


        # Définition des Widgets
        materielWidget=CheckList();
        try:
            parent.DB.execute("select nom from Materiel")
            for result in parent.DB:
                materielWidget.addItem(result[0])
        except:
            parent.statusBar().showMessage('Echec de connexion à la DB')
            materielWidget.addItems(['Poids', 'Elastique', 'Barre traction', 'Corde à sauter', 'Escaliers'])

        personneWidget=qtw.QComboBox();
        personneWidget.addItems(['-', '1', '2', '3'])

        # Définition du layout
        layout=qtw.QGridLayout()
        layout.addWidget(qtw.QLabel('Matériel à disposition'), 0,0)
        layout.addWidget(materielWidget, 1,0, 3,0)
        layout.addWidget(qtw.QLabel('Nombre de participants'), 0,2)
        layout.addWidget(personneWidget, 1,2)

        self.setLayout(layout)

class CaracPerso(qtw.QWidget):

    def __init__(self, parent=None):

        # Constructeur parent
        qtw.QWidget.__init__(self)
        self.parent = parent

        # Définition des Widgets
        self.levelWidget=qtw.QComboBox()
        self.levelWidget.addItems(['1', '2', '3'])
        blessureWidget=qtw.QComboBox()
        try:
            parent.DB.execute("select nom from Muscle")
            blessureWidget.addItem('-')
            for result in parent.DB:
                blessureWidget.addItem(result[0])
        except:
            parent.statusBar().showMessage('Echec de connexion à la DB')
            blessureWidget.addItems(['-', 'Bras', 'Torse', 'Dorsaux', 'Fessiers', 'Jambe', 'Petit orteil'] )

        self.levelWidget.activated[str].connect(self.onActivated)
        # Définition du layout
        layout=qtw.QGridLayout()
        layout.addWidget(qtw.QLabel('Niveaux'), 0,0)
        layout.addWidget(self.levelWidget, 1,0)
        layout.addWidget(qtw.QLabel('Blessure'), 0,1)
        layout.addWidget(blessureWidget, 1,1)
        self.setLayout(layout)

    def onActivated(self, level):
        self.parent.level = level

class ZoneBouton(qtw.QWidget):

    def __init__(self, parent= None):
        qtw.QWidget.__init__(self)
        self.parent = parent
        self.button1 = qtw.QPushButton('Send my training')
        self.button2 = qtw.QPushButton('Close')
        self.button2.clicked.connect(parent.close)
        self.button1.clicked.connect(self.lanceFenetre)
        layout = qtw.QHBoxLayout()
        layout.addWidget(self.button2)
        layout.addWidget(self.button1)
        self.setLayout(layout)

    def lanceFenetre(self):
        self.connexion = intro.IntroWindow(parent = self.parent)

class zoneChoix(qtw.QWidget):

    def __init__(self, parent=None):

        # Appel du constructeur parent
        qtw.QWidget.__init__(self)
        
        # Titre
        titleLabel1=qtw.QLabel("Caractéristiques de l'entrainement")
        titleLabel1.setFont(QtGui.QFont("Arial", 14, QtGui.QFont.Black))
        titleLabel2=qtw.QLabel("Environnement de travail")
        titleLabel2.setFont(QtGui.QFont("Arial", 14, QtGui.QFont.Black))
        titleLabel3=qtw.QLabel("Caractéristiques personnelles")
        titleLabel3.setFont(QtGui.QFont("Arial", 14, QtGui.QFont.Black))

        # Boutons
        boutonValider = qtw.QPushButton("Make my training")
        boutonValider.clicked.connect(parent.makeTraining)

        # Logo
        logo = qtw.QLabel()
        logo.setPixmap(QtGui.QPixmap('./logo/intro.png'))

        # Définition du layout
        layout = qtw.QVBoxLayout()
        layout.addWidget(logo)
        layout.addWidget(titleLabel1)
        layout.addWidget(CaracEntrainement(parent = parent))
        layout.addWidget(titleLabel2)
        layout.addWidget(CaracEnv(parent = parent))
        layout.addWidget(titleLabel3)
        layout.addWidget(CaracPerso(parent = parent))
        layout.addWidget(boutonValider)
        layout.addWidget(ZoneBouton(parent= parent))
        self.setLayout(layout)

class CentralWindow(qtw.QWidget):

    def __init__(self, parent=None):

        # Appel du constructeur parent
        qtw.QWidget.__init__(self)

        # Définition du layout
        layout = qtw.QHBoxLayout()
        layout.addWidget(zoneChoix(parent = parent))
        if POPPLER_STATE:
            self.PDFArea = ScrollPDFArea()
            layout.addWidget(self.PDFArea)
        self.setLayout(layout)

        # Exit Action
        exitAction=qtw.QAction('&Exit', self)
        exitAction.setShortcut('Ctrl-Q')
        exitAction.setStatusTip("Quitter l'application")
        exitAction.triggered.connect(qtw.qApp.exit)

class MainFenetre(qtw.QMainWindow):

    def __init__(self):

        # Appel constructeur de la classe parent
        qtw.QMainWindow.__init__(self)
        self.setWindowIcon(QtGui.QIcon('logo/logo.png'))
        # Chargement de l'interface
        #self.intro = IntroWindow()
        #self.intro.show()
        self.loadUI()
        #self.intro.close()
        self.show()


    def loadUI(self):
        # Attributs
        self.DB = None
        self.connectDB()

        # Critères
        self.muscleId = []
        self.serieId = []
        self.serieNom = []
        self.mouvement = []
        self.repet = []
        self.level=1

        # Caractéristiques de la fenêtre
        self.setWindowTitle("Fitness Training (beta)")
        self.centralWidget = CentralWindow(parent=self)

        # Init du bandeau menu
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('File')
        editMenu = mainMenu.addMenu('Edit')
        viewMenu = mainMenu.addMenu('View')
        searchMenu = mainMenu.addMenu('Search')
        toolsMenu = mainMenu.addMenu('Tools')
        helpMenu = mainMenu.addMenu('Help')
        exitButton = qtw.QAction('Exit', self)

        # Définition des actions du menuBar
        exitButton.setShortcut('Ctrl+Q')
        exitButton.setStatusTip('Exit application')
        exitButton.triggered.connect(self.close)
        fileMenu.addAction(exitButton)

        #Définition de la fenetre de travail
        self.setCentralWidget(self.centralWidget)




    def makeTraining(self):
        self.mouvement = []
        self.repet = []
        self.serieId = []
        self.statusBar().showMessage('Récupération des données de la BDD...')
        requete ='select Serie.id from Serie inner join Mouvement on Serie.mouvement=Mouvement.id inner join Muscle on Mouvement.muscle=Muscle.id where'
        requete += ' Serie.niveaux=' + str(self.level) + ' and'
        for i in self.muscleId:
            if i==self.muscleId[-1]:
                requete += ' Muscle.id=' + str(i)
            else :
                requete += ' Muscle.id=' + str(i) + ' or'
        self.DB.execute(requete)
        for result in self.DB:
            self.serieId.append(result[0])
        for i in self.serieId:
            self.DB.execute('select Mouvement.nom, Serie.nbRepetitions from Serie inner join Mouvement on Serie.mouvement=Mouvement.id where Serie.id='+str(i))
            for result in self.DB:
                self.mouvement.append(result[0])
                self.repet.append(result[1])
        print (self.mouvement)
        print(self.repet)
        self.statusBar().showMessage('Compilation du PDF...')
        self.render()

    def render(self):
        try:
            os.remove('./out/training.tex')
        except :
            pass
        fichier = open('./out/training.tex', 'x')
        fichier.write(r'''\documentclass[french]{article}
\usepackage[utf8x]{inputenc}
\usepackage{titling}
\title{Brandy's fitness training}
\author{ auto-genrated by Brandy}

\begin{document}
\maketitle

\begin{itemize}
''')
        for i in range (len(self.mouvement)):
            fichier.write(r'\item ' + str(self.repet[i]) + r' $\times$ ' + self.mouvement[i] + '\n')
        fichier.write(r'''
\end{itemize}
\end{document} ''')
        fichier.close()
        os.chdir('./out/')
        os.system('pdflatex training.tex')
        os.chdir('../')
        if POPPLER_STATE:
            self.centralWidget.PDFArea.update()
        else:
            os.startfile('training.pdf')
        self.statusBar().showMessage('Prêt')

    def launchMuscleWindow(self, items=None):
        self.ex = MusclePopUp(items=items, parent=self)

    def connectDB(self):
        self.statusBar().showMessage('Connexion à la DB...')
        try :
            self.conn = db.connect(host='176.184.211.155', user='admin', password='NicolasSully', database='fitnessDB')
            self.DB = self.conn.cursor()
            self.statusBar().showMessage('Connecté à la BDD...prêt !')
        except :
            self.statusBar().showMessage('Echec de connexion à la BDD')
